<?php
header('charset=utf-8');
session_start();


/********************************************************

		Mikrotik Limiter
		W/ Mikrotik API
		By: Plósz János @ 2015.07.16


http://bitbucket.org/MadCatzAno/mikrotik-limiter
********************************************************/


//Api meghivasa
require_once('api.php');
$API = new routeros_api();

/*
Csatlakozási tábla. 
1 Array = 1 login. (ip,user,pass)
*/
$connect = Array(
Array("IP","USER","PASS"),
);


/*
Custom parancslista. Csak az array-ban foglaltakat írja ki.
Alap location: ?wireless
*/
$lista = Array(
	"/interface/wireless/print" => Array("name",
		"mac-address",
		"ssid",
		"frequency-mode",
		"country",
		"frequency",
		"band",
		"channel-width",
		"scan-list",
		"wireless-protocol",
		"wds-mode",
		"wds-default-bridge",
	),
	"/interface/wireless/registration-table/print" => Array(
		"mac-address",
		"signal-to-noise",
		"signal-strength-ch0",
		"signal-strength-ch1",
		"tx-ccq",
		"tx-rate",
		"rx-rate"
	),

/*
Ha az Array első eleme "off", akkor nem íródik ki alap esetben.
*/

	"/interface/ethernet/print" => Array(
		"off",
		"name",
		"default-name",
		"mac-address",
		"speed",
		"tx-too-short",
		"tx-too-long",
		"rx-too-short",
		"rx-too-long",
		"rx-fcs-error",
		"rx-align-error",
		"rx-fragment",
		"rx-overflow",
		"tx-collision",
		"tx-excessive-collision",
		"tx-multiple-collision",
		"tx-single-collision",
		"tx-excessive-deferred",
		"tx-deferred",
		"tx-late-collision",
		"running",
		"slave",
		"disabled"
	),

);


/*
A cmd GET paraméterei, ami az indexeket dolgozza fel.
ha a parancs _ -al kezdödik, a fenti custom parancs listából írja ki a megfelelö adatokat a parancsról.
*/
$parancsok = Array(
	"0" => "/system/resource/print",
	"1" => "/system/reboot",
	"2" => "_/interface/ethernet/print",
);



/*
Ha nincs session, egy login form, és annak feldolgozása
*/
if(!isset($_SESSION["mikrotik"]["user"])) {
	echo '<html><head><title>Bejelentkezes</title></head><body><form method="post"><table style="width:500px;">
	<tbody>
	<tr>
	<td>IP</td>
	<td><select required name="ip"><option>Válassz...</option>'; 

	//Csak az elöre definiált IP címeket kínáljuk fel.
	foreach($connect as $con) {
	echo '<option value="'.$con[0].'">'.$con[0].'</option>';
	}
	echo '</select></td>

	</tr>

	<tr>
	<td>User</td>
	<td><input type="text" name="user" required placeholder="User" /></td>
	</tr>

	<tr>
	<td>Jelszó</td>
	<td><input type="password" name="pass" required placeholder="Jelszo" /></td>
	</tr>

	<tr>
	<td> </td>
	<td><input type="submit" value="Bejelentkezés" name="submiT" /></td>
	</tr>

	</tbody>
	</table></form></body></html>';

	//Login form feldolgozása.
	if(isset($_POST["submiT"])) {
		if($_POST["ip"] && $_POST["user"] && $_POST["pass"]) {
			foreach($connect as $con) {
				$test = Array($_POST["ip"], $_POST["user"], $_POST["pass"]);
				if($con == $test) {
				$_SESSION["mikrotik"]["ip"] = $_POST["ip"];
				$_SESSION["mikrotik"]["user"] = $_POST["user"];
				$_SESSION["mikrotik"]["pass"] = $_POST["pass"];
				header("Location: ?");
				die();
				}
			}
		echo "<b style='color:#ff0000'>Bejelentkezés sikertelen!</b>";
		}
	}

} else {
	echo '<html>
	<head>
	<style>
		.button {
		font: bold 11px Arial;
		text-decoration: none;
		background-color: #EEEEEE;
		color: #333333;
		padding 4px 6px 4px;
		border-top 1px solid #CCCCCC;
		border-right 1px solid #333333;
		border-botton 1px solid #333333;
		border-left: 1px solid #CCCCCC;
		}
	</style>
	<title>Info</title>
	<meta charset="UTF-8">
	<script>

//"Hülyebiztos" ajax-os újratöltés
function showData(url) {
	var changing = 0;
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
	if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		document.getElementById("data").innerHTML = xmlhttp.responseText;
		changing = 0;
	}
	}
	if(changing == 0) {
		document.getElementById("data").innerHTML = "Betöltés...";
		xmlhttp.open("GET",url,true);
		xmlhttp.send();
		changing = 1;
	}
}
</script>
</head>
<body>';
	if ($API->connect($_SESSION["mikrotik"]["ip"], $_SESSION["mikrotik"]["user"], $_SESSION["mikrotik"]["pass"])) {


		if(!isset($_GET["cmd"]) && !isset($_GET["wireless"])) {
			echo "<b>Csatlakozva: ".$_SESSION["mikrotik"]["user"]."@". $_SESSION["mikrotik"]["ip"]."</b><br/>";
			echo '
			<table>
			<tbody>
			<tr>
			<td><a class="button" href="#" onclick="showData(\'?wireless\');">Wireless fül</a></td>
			<td>|</td>
			<td><a class="button" href="#" onclick="showData(\'?cmd=0\')">Resource print</a></td>
			<td>|</td>
			<td><a class="button" href="#" onclick="showData(\'?cmd=2\')">Error lista</a></td>
			<td>|</td>
			<td><a class="button" href="#" onlcick="showData(\'?cmd=1\')">Reboot</a></td>
			<td>|</td>
			<td><a class="button" href="?logout">Kijelentkezés</a></td>
			</tr>
			</tbody>
			</table>
			<div id="data">.</div>
			';
		}


		if(isset($_GET["logout"])) {
			session_destroy();
			header("Location: ?");
		}

		//cmd GET feldolgozása a aprancsok listából
		if(isset($_GET["cmd"])) {
			if(!empty($parancsok[$_GET["cmd"]])) {
				if(substr($parancsok[$_GET["cmd"]],0,1) == "_") {
					$command = explode("_", $parancsok[$_GET["cmd"]]);
					$command = $command[1];
				} else {
					$command = $parancsok[$_GET["cmd"]];
			}

		$API->write($command);
 		$ARRAY = $API->read();
		echo "<h2>".$command."</h2>";

		foreach($ARRAY as $key => $value) {
			echo "<hr/>";
			echo '<br/><table border="1" style="width: 500px;"><tbody>';
			if(substr($parancsok[$_GET["cmd"]],0,1) == "_") {
				$listacmd = explode("_", $parancsok[$_GET["cmd"]]);
				$lista2 = $lista[$listacmd[1]];


				$color = "#EEEEEE"; $font = "#000000";
				foreach($lista2 as $val) {
					if($val == "off") { continue; }
					if($color == "#EEEEEE") { $color="#666666"; $font = "#FFFFFF"; } else { $color="#EEEEEE"; $font="#000000"; }
					echo "<tr style='background-color:".$color."; color:".$font."'><td>".$val."</td> <td>". $value[$val]."</td></tr>";

				}

			} else {
				$color = "#EEEEEE"; $font = "#000000";
				foreach($value as $key2 => $v2) {
					if($color == "#EEEEEE") { $color="#666666"; $font = "#FFFFFF"; } else { $color="#EEEEEE"; $font="#000000"; }
					echo "<tr style='background-color:".$color."; color:".$font."'><td>".$key2."</td><td>" . $v2."</td></tr>";
				}
			}

		echo "</tbody></table>";
		}
	}
}


//Ethernet print test.

/*
if(isset($_GET["test"])) {
	$API->write("/interface/ethernet/print");
	$ARRAY = $API->read();

	foreach($ARRAY as $key => $value) {
		foreach($value as $key2 => $v2) {
			echo $key2.  "=" . $v2."<br/>";
		}
	}
	$API->disconnect();
	die();
}
*/

if(isset($_GET["wireless"])) {
	echo "<h2>Wireless fül</h2>";
	foreach($lista as $command => $list) {
		$API->write($command);
		$ARRAY = $API->read();

		//var_dump($command);
		//var_dump($ARRAY);
			$temp_command = "";
			foreach($ARRAY as $key => $value) {
				if($list[0] == "off") { continue; }
				if($temp_command != $command) { echo $command."<hr/>"; $temp_command = $command; }
				echo '<br/><table border="1" style="width: 500px;"><tbody>';
					if($list[0] == "all") {
						foreach($value as $key2 => $v2) {
							echo $key2.  "=" . $v2."<br/>";
						}

					} else if($list[0] != "off"){
						$color = "#EEEEEE"; $font = "#000000";
						foreach($list as $val) {
							if($color == "#EEEEEE") { $color="#666666";$font="#FFFFFF"; } else { $color="#EEEEEE"; $font="#000000"; }
							echo "<tr style='background-color:".$color.";color:".$font."'><td>".$val."</td> <td>". $value[$val]."</td></tr>";
						}
					}
				echo '</tbody></table>';

			}
	}
	$API->disconnect();
}

} else {
	echo "Csatlakozas sikertelen: ".$_SESSION["mikrotik"]["user"]."@".$_SESSION["mikrotik"]["ip"]." .";
}

echo '</body></head>';
}
?>
